using System;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ChatClient.Services
{
    public class JsonService : IJsonService
    {
        /// <summary>
        ///     Deserialize a JSON string into an object of a given type.
        /// </summary>
        /// <typeparam name="T">Type of the object to get.</typeparam>
        /// <param name="json">Json string to deserialize into an object.</param>
        /// <returns>Deserialized JSON object</returns>
        public async Task<T> GetDeserializedResponse<T>(string json)
        {
            try
            {
                return await Task.Run(() => JsonConvert.DeserializeObject<T>(json)).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
                return default(T);
            }
        }

        /// <summary>
        ///     Serialize an object into a JSON string.
        /// </summary>
        /// <param name="objectToConvert">Object to serialize into a string.</param>
        /// <returns>Json serialization of the object.</returns>
        public async Task<string> GetSerializedResponse(object objectToConvert)
        {
            try
            {
                return await Task.Run(() => JsonConvert.SerializeObject(objectToConvert)).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
                return string.Empty;
            }
        }
    }
}