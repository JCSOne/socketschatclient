using System.Threading.Tasks;

namespace ChatClient.Services
{
    public interface IJsonService
    {
        Task<T> GetDeserializedResponse<T>(string json);
        Task<string> GetSerializedResponse(object objectToConvert);
    }
}