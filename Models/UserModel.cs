using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;
using ChatClient.Services;

namespace ChatClient.Models
{
    public class UserModel
    {
        private StreamReader _reader;
        private StreamWriter _writer;
        private JsonService _jsonService;

        public UserModel(TcpClient server)
        {
            _reader = new StreamReader(server.GetStream());
            _writer = new StreamWriter(server.GetStream());
            _jsonService = new JsonService();
        }

        public async Task<bool> Register(User user)
        {
            _writer.WriteLine($":register:{await _jsonService.GetSerializedResponse(user)}");
            _writer.Flush();
            var response = await _reader.ReadLineAsync();
            if (bool.TryParse(response, out var registered))
                return registered;
            Console.WriteLine(response);
            return false;
        }

        public async Task<bool> Login(User user)
        {
            _writer.WriteLine($":login:{await _jsonService.GetSerializedResponse(user)}");
            _writer.Flush();
            var response = await _reader.ReadLineAsync();
            if (bool.TryParse(response, out var loggedIn))
                return loggedIn;
            Console.WriteLine(response);
            return false;
        }

        public IEnumerable<User> All()
        {
            return new List<User>();
        }
    }
}