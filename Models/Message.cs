using System;

namespace ChatClient.Models
{
    public class Message
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime SentDate { get; set; }
        public User Sender { get; set; }
        public User Recipient { get; set; }
    }
}