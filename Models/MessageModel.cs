using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;
using ChatClient.Services;

namespace ChatClient.Models
{
    public class MessageModel
    {
        private StreamReader _reader;
        private StreamWriter _writer;
        private JsonService _jsonService;

        public MessageModel(TcpClient server)
        {
            _reader = new StreamReader(server.GetStream());
            _writer = new StreamWriter(server.GetStream());
            _jsonService = new JsonService();
        }

        public async Task SendMessage(Message message)
        {
            _writer.WriteLine(await _jsonService.GetSerializedResponse(message));
            _writer.Flush();
        }

        public IEnumerable<Message> GetMessages()
        {
            return new List<Message>();
        }
    }
}