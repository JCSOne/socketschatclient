﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ChatClient.Models;

namespace ChatClient
{
    class Program
    {
        private static TcpClient _server = new TcpClient();
        private static UserModel _userModel;
        private static bool _loggedIn = false;

        static void Main(string[] args)
        {
            try
            {
                _server = new TcpClient("localhost", 8888);
                _userModel = new UserModel(_server);

                StreamReader reader = new StreamReader(_server.GetStream());
                new Thread(() =>
                {
                    try
                    {
                        while (_server.Connected)
                        {
                            if (!_loggedIn)
                                continue;
                            var line = reader.ReadLine();
                            Console.WriteLine(line);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Error1: {e.Message}");
                    }
                }).Start();
                Task.Run(SendMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine($"There was an error connecting to the server - ({e.Message})");
            }
        }

        private static async Task SendMessage()
        {
            try
            {
                while (true)
                {
                    if (!_loggedIn)
                    {
                        Console.WriteLine("Por favor registrate o inicia sesión para continuar");
                        Console.WriteLine("Selecciona la tarea que deseas realizar: ");
                        Console.WriteLine("1. Registrar");
                        Console.WriteLine("2. Iniciar Sesión");

                        if (int.TryParse(Console.ReadLine(), out var option))
                        {
                            if (option == 1)
                            {
                                Console.WriteLine("Ingresa tu nombre");
                                var name = Console.ReadLine();
                                Console.WriteLine("Ingresa tu nickname");
                                var nickname = Console.ReadLine();
                                Console.WriteLine("Ingresa tu contraseña");
                                var password = Console.ReadLine();
                                Console.WriteLine("Confirma tu contraseña");
                                var passwordConfirmation = Console.ReadLine();
                                var user = new User()
                                {
                                    Name = name,
                                    Nickname = nickname,
                                    Password = password,
                                    PasswordConfirmation = passwordConfirmation
                                };
                                var registration = await _userModel.Register(user);
                                if (registration)
                                {
                                    Console.WriteLine("Registro exitoso");
                                    _loggedIn = true;
                                }
                            }
                            else if (option == 2)
                            {
                                Console.WriteLine("Ingresa tu nickname");
                                var nickname = Console.ReadLine();
                                Console.WriteLine("Ingresa tu contraseña");
                                var password = Console.ReadLine();
                                var user = new User()
                                {
                                    Nickname = nickname,
                                    Password = password
                                };
                                var login = await _userModel.Login(user);
                                if (login)
                                {
                                    Console.WriteLine("Login Exitoso");
                                    _loggedIn = true;
                                }
                            }
                            else
                            {
                                Console.WriteLine("La opción seleccionada no es válida");
                            }
                            continue;
                        }
                    }
                    var writer = new StreamWriter(_server.GetStream());
                    var line = Console.ReadLine();
                    writer.WriteLine(line);
                    writer.Flush();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error2: {e.Message}");
            }
        }
    }
}
